#include "transform.h"

#pragma region Constructors
mgd::Transform::Transform(const Vector3& translate, const Quaternion& rotate , Scalar scale ) : translate(translate), rotate(rotate), scale(scale) {}
#pragma endregion Constructors

mgd::Vector3 mgd::Transform::transformPoint(const Vector3& p) const
{
	return rotate.apply(p * scale) + translate;
}

mgd::Vector3 mgd::Transform::transformVersor(const Vector3& p) const
{
	return rotate.apply(p);
}

mgd::Vector3 mgd::Transform::transformVector(const Vector3& p) const
{
	return rotate.apply(p * scale);
}

mgd::Scalar mgd::Transform::transformScalar(Scalar p) const
{
	return p * scale;
}

mgd::Transform mgd::Transform::inverse() const
{
	Transform t;
	t.scale = (1 / scale);
	t.rotate = rotate.conjugated();
	t.translate = t.rotate.apply(-translate * t.scale); // oppure: t.translarte = t.applyToVector( -translate );
	return t;
}

void mgd::Transform::invert()
{
	scale = (1 / scale);
	rotate.conjugate();
	translate = rotate.apply(-translate * scale); // oppure: translate = applyToVector(-translate);
}

void mgd::Transform::move(Vector3 offset)
{
	this->translate += offset;
}

void mgd::Transform::turn(Quaternion offset)
{
	this->rotate = offset * rotate;
}

mgd::Vector3 mgd::Transform::forward() const
{
	return rotate.apply(Vector3::forward());
}

mgd::Vector3 mgd::Transform::right() const
{
	return rotate.apply(Vector3::right());
}

mgd::Vector3 mgd::Transform::up() const
{
	return rotate.apply(Vector3::up());
}

mgd::Transform mgd::Transform::lerp(const Transform& a, const Transform& b, float t)
{
	return Transform(Vector3::lerp(a.translate, b.translate, t), Quaternion::lerp(a.rotate, b.rotate, t), mgd::lerp(a.scale, b.scale, t));
}