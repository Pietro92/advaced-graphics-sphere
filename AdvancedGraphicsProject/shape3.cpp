#include "shape3.h"
#include "gameobject.h"
#include "transform.h"

#pragma region Constructors
mgd::Sphere::Sphere(Point3 nCenter, Scalar nRadius, Transform nTransform) : GameObject(nTransform), center(nCenter), radius(nRadius) {}

mgd::Plane::Plane(Point3 nPoint, Versor3 nNormal, Transform nTransform) : GameObject(nTransform), point(nPoint), normal(nNormal) {}
#pragma endregion Constructors

#pragma region Sphere Override
void mgd::Sphere::rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const
{
	mgd::rayCast(ray, *this, hitPos, hitNorm, distMax);
}

mgd::GameObject* mgd::Sphere::applyTransform(const Transform& nTransform) const
{
	return new Sphere(nTransform.transformPoint(center), nTransform.transformScalar(radius));
}
#pragma endregion Sphere Override

#pragma region Plane Override
void mgd::Plane::rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const
{
	mgd::rayCast(ray, *this, hitPos, hitNorm, distMax);
}

mgd::GameObject* mgd::Plane::applyTransform(const Transform& nTransform) const
{
	return new Plane(nTransform.transformPoint(point), normal);
}
#pragma endregion Plane Override