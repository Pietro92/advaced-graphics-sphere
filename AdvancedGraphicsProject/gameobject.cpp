#include "gameobject.h"

#pragma region Constructors
mgd::GameObject::GameObject(Transform nTransform) : transform(nTransform) {}
#pragma endregion Constructors

#pragma region Destructor
mgd::GameObject::~GameObject() {}
#pragma endregion Destructor