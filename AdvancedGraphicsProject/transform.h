#pragma once
#include <iostream>
#include "Vector3.h"
#include "Quaternion.h"

namespace mgd {
	class Transform {
	public:
		Scalar scale;
		Vector3 translate;
		Quaternion rotate;

	public:
		Transform(const Vector3& translate = Vector3(), const Quaternion& rotate = Quaternion::identity(), Scalar scale = 1);;

		Vector3 transformPoint(const Vector3& p) const;
		Vector3 transformVersor(const Vector3& p) const;
		Vector3 transformVector(const Vector3& p) const;
		Scalar transformScalar(Scalar p) const;

		Transform inverse() const;
		void invert();
		void move(Vector3 offset);
		void turn(Quaternion offset);

		Vector3 forward() const;
		Vector3 right() const;
		Vector3 up() const;

		static Transform lerp(const Transform& a, const Transform& b, float t);

		friend std::ostream& operator<<(std::ostream& os, Transform const& value)
		{
			os << "Current translate: (" << value.translate.X << ", " << value.translate.Y << ", " << value.translate.Z << ")" << std::endl
				<< "Current rotation: (" << value.rotate.im.X << ", " << value.rotate.im.Y << ", " << value.rotate.im.Z << ", " << value.rotate.re << ")" << std::endl
				<< "Current scale: " << value.scale << std::endl;
			return os;
		}
	};

	inline Transform operator*(const mgd::Transform& a, const mgd::Transform& b)
	{
		mgd::Transform t;
		t.rotate = a.rotate * b.rotate;
		t.scale = a.scale * b.scale;
		t.translate = a.transformVector(b.translate) + a.translate;
		return t;
	}
}