#pragma once
#include "Vector3.h"

namespace mgd
{
	class Transform;
	class Quaternion;

	struct Ray;
	class Plane;
	class Sphere;

	namespace test
	{
		void unitTestLinearOps()
		{
			Vector3 a(24, 1, 32), b(12, 3, 54);
			Scalar k = 5.0;
			assert(areEqual(a + b, b + a));
			assert(areEqual((a + b) * k, b * k + a * k));
		}

		void unitTestProdutcs()
		{
			Vector3 a(24, -3, 32), b(12, 3, -54), c(10, 13, -43);
			Scalar k = 0.4f;

			assert(isZero(Vector3::dot(a, Vector3::cross(a, b))));
			assert(isZero(Vector3::dot(b, Vector3::cross(a, b))));

			assert(isZero(Vector3::cross(a, b) + Vector3::cross(b, a)));

			assert(areEqual(Vector3::dot(k * a, b), Vector3::dot(b, k * a)));
			assert(areEqual(Vector3::cross(a, b + 4 * c), Vector3::cross(a, b) + 4 * Vector3::cross(a, c)));
			assert(areEqual(Vector3::cross(2 * a, 2 * b), 4 * Vector3::cross(a, b)));
		}

		void unitTestRaycasts()
		{
			Ray ray = Ray(Point3::zero(), Vector3::right());
			Sphere sphere = Sphere(Point3(5, 0, 0), 3);
			Point3 hitPoint;
			Point3 hitNorm;
			Scalar distMax = 100000;

			bool isHit = rayCast(ray, sphere, hitPoint, hitNorm, distMax);
			assert(isHit);
			assert(areEqual(hitPoint, Point3(2, 0, 0)));
		}

		void unitTestRaycastPlane()
		{
			Ray r(Point3(0, 0, 0), Vector3(1, 0, 0));
			Plane p(Point3(10, 0, 0), Vector3(-1, 0, 0));
			Point3 hitPoint;
			Point3 hitNorm;
			Scalar distMax = 100000;

			bool isHit = rayCast(r, p, hitPoint, hitNorm, distMax);
			assert(isHit);
			assert(areEqual(hitPoint, Point3(10, 0, 0)));
		}

		void examplesOfSyntax()
		{
			Vector3 v(0, 2, 3);

			Vector3 r = v * 4;  //Vector3 r = v.operator *(4);
			r *= 0.25;
			Vector3 w = v + r; //  Vector3 w = v.operator + (r);
			v += w;

			v = r - v;
			v = -w + v;
			Scalar k = Vector3::dot(v, w);

			v.X = 0.4f;

			Scalar h = v[0];

			// v[6] = 0.3;
		}

		void unitTestQuaternions()
		{
			{
				Quaternion rot = Quaternion::fromAngleAxis(180, Vector3(0, 1, 0));
				Vector3 p(0, 0, 1);
				Vector3 q = rot.apply(p);
				assert(areEqual(q, Vector3(0, 0, -1)));
			}

			{
				Quaternion rot = Quaternion::fromAngleAxis(90, Vector3(0, 1, 0));
				Vector3 p(3, 5, 6);
				Vector3 q = rot.apply(p);
				assert(areEqual(q, Vector3(6, 5, -3)));
			}


			Quaternion rot = Quaternion::fromAngleAxis(30, Vector3(1, 1, 3));
			Vector3 p(3, 5, 6), q = p;
			for (int k = 0; k < 12; k++) q = rot.apply(q);
			assert(areEqual(q, p));



			{
				Quaternion q = Quaternion::identity();

				Vector3 randomAxis(4, 3, 1);
				Scalar deg = 90.f;

				Quaternion qRot = Quaternion::fromAngleAxis(deg, randomAxis);

				for (int i = 0; i < 8; i++) {
					q = q * qRot;
				}

				assert(areEquivalent(q, Quaternion::identity()));
				assert(areEqual(q, Quaternion::identity()));
			}
		}

		void unitTestTransformation()
		{
			Transform t;
			t.rotate = Quaternion::fromAngleAxis(43.4f, Vector3(1, -3, -2));
			t.translate = Vector3(1, 3, 4);
			t.scale = 5;

			Point3 p(4, 10, -13);

			Point3 q = t.transformPoint(p);
			Point3 r = t.inverse().transformPoint(q);

			assert(areEqual(p, r));

			Transform ti = t;
			ti.invert();
			r = ti.transformPoint(q);
			assert(areEqual(p, r));

			Transform tA;
			tA.rotate = Quaternion::fromAngleAxis(-13.4f, Vector3(13, 4, 0));
			tA.translate = Vector3(0, -1, 01);
			tA.scale = 0.23f;

			Transform tB = t;

			Transform tAB = tA * tB;
			assert(areEqual(
				tAB.transformPoint(p),
				tA.transformPoint(tB.transformPoint(p))
			)
			);
		}

		void runUnitTests()
		{
			unitTestLinearOps();
			unitTestProdutcs();
			unitTestRaycasts();
			unitTestRaycastPlane();
			unitTestQuaternions();
			unitTestTransformation();
		}
	}
}