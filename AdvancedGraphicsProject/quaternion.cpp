#include "quaternion.h"

#pragma region Constructors
mgd::Quaternion::Quaternion(Vector3 _im, Scalar _re) :im(_im), re(_re) {}
#pragma endregion Constructors

#pragma region Operations
mgd::Quaternion mgd::Quaternion::operator+(Quaternion rhs) const
{
	return Quaternion(this->im + rhs.im, this->re + rhs.re);
}

mgd::Quaternion mgd::Quaternion::operator*(mgd::Scalar s) const
{
	return Quaternion(im * s, re * s);
}

mgd::Quaternion mgd::Quaternion::operator/(mgd::Scalar s) const
{
	return Quaternion(im / s, re / s);
}
#pragma endregion Operations

mgd::Vector3 mgd::Quaternion::apply(Vector3 p) const
{
	Quaternion q = fromVector3(p);
	q = (*this) * q * this->conjugated();
	assert(areEqual(q.re, 0));
	return q.im;
}

void mgd::Quaternion::conjugate()
{
	im = -im;
}

mgd::Quaternion mgd::Quaternion::conjugated() const
{
	return Quaternion(-im, re);
}

mgd::Quaternion mgd::Quaternion::inverse() const
{
	return conjugated() / squaredNorm();
}

mgd::Quaternion mgd::Quaternion::operator-() const
{
	return Quaternion(-im, -re);
}

mgd::Scalar mgd::Quaternion::squaredNorm() const
{
	return re * re + im.squaredNorm();
}

mgd::Scalar mgd::Quaternion::norm() const
{
	return std::sqrt(squaredNorm());
}

mgd::Quaternion mgd::Quaternion::fromVector3(Vector3 v)
{
	return Quaternion(v, 0);
}

mgd::Quaternion mgd::Quaternion::fromAngleAxis(Scalar deg, Vector3 axis)
{
	Scalar rad = deg * (float)M_PI / 180;
	Scalar s = std::sin(rad / 2);
	Scalar c = std::cos(rad / 2);
	return Quaternion(axis.normalized() * s, c);
}

mgd::Quaternion mgd::Quaternion::identity()
{
	return Quaternion(Vector3(0, 0, 0), 1);
}

float mgd::Quaternion::angleBetween(const Quaternion& a, const Quaternion& b)
{
	Quaternion q = b * a.inverse();
	float angle = 2.f * std::asin(q.im.norm());
	return angle;
}

mgd::Quaternion mgd::Quaternion::lerp(const Quaternion& a, const Quaternion& b, float t)
{
	return Quaternion(Vector3::lerp(a.im, b.im, t), mgd::lerp(a.re, b.re, t));
}

mgd::Quaternion mgd::Quaternion::slerp(const Quaternion& a, const Quaternion& b, float t)
{
	float angle = angleBetween(a, b);
	return (a * (std::sin((1 - t) * angle) / std::sin(angle))) + (b * (std::sin(t * angle) / std::sin(angle)));
}