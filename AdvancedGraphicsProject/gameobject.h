#pragma once
#include "Transform.h"

namespace mgd {
	struct Ray;

	class GameObject {
	public:
		Transform transform;

	public:
		GameObject(Transform nTransform = Transform());
		virtual ~GameObject();

		virtual void rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const = 0;
		virtual GameObject* applyTransform(const Transform& t) const = 0;
	};
}
