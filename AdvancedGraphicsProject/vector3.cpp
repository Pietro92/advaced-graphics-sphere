#include "vector3.h"

using namespace mgd;

#pragma region Constructors
Vector3::Vector3() :X(0), Y(0), Z(0) {}

Vector3::Vector3(float _x, float _y, float _z) : X(_x), Y(_y), Z(_z) {}
#pragma endregion Constructors

#pragma region Operations
mgd::Vector3 Vector3::operator+() const
{
	return Vector3(+X, +Y, +Z);
}

mgd::Vector3 Vector3::operator-() const
{
	return Vector3(-X, -Y, -Z);
}

void Vector3::operator-=(const Vector3& v)
{
	X -= v.X;
	Y -= v.Y;
	Z -= v.Z;
}

mgd::Vector3 Vector3::operator-(const Vector3& v) const
{
	return Vector3(X - v.X, Y - v.Y, Z - v.Z);
}

void Vector3::operator+=(const Vector3& v)
{
	X += v.X; Y += v.Y; Z += v.Z;
}

mgd::Vector3 Vector3::operator+(const Vector3& v) const
{
	return Vector3(X + v.X, Y + v.Y, Z + v.Z);
}

void Vector3::operator/=(Scalar k)
{
	X /= k;
	Y /= k;
	Z /= k;
}

mgd::Vector3 Vector3::operator/(Scalar k) const
{
	return Vector3(X / k, Y / k, Z / k);
}

void Vector3::operator*=(Scalar k)
{
	X *= k;
	Y *= k;
	Z *= k;
}

mgd::Vector3 Vector3::operator*(Scalar k) const
{
	return Vector3(k * X, k * Y, k * Z);
}
#pragma endregion Operations

#pragma region Other Operations
void Vector3::normalize()
{
	(*this) /= norm();
}

Vector3 Vector3::normalized() const
{
	return (*this) / norm();
}

mgd::Scalar Vector3::norm() const
{
	return std::sqrt(squaredNorm());
}

mgd::Scalar Vector3::squaredNorm() const
{
	return X * X + Y * Y + Z * Z;
}

mgd::Vector3 Vector3::lerp(const Vector3& v1, const Vector3& v2, float delta)
{
	return Vector3(mgd::lerp(v1.X, v2.X, delta), mgd::lerp(v1.Y, v2.Y, delta), mgd::lerp(v1.Z, v2.Z, delta));
}

mgd::Scalar Vector3::dot(const Vector3& v1, const Vector3& v2)
{
	return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
}

mgd::Vector3 Vector3::cross(const Vector3& v1, const Vector3& v2)
{
	float a = v1.Y * v2.Z - v1.Z * v2.Y;
	float b = v1.Z * v2.X - v1.X * v2.Z;
	float c = v1.X * v2.Y - v1.Y * v2.X;
	return Vector3(a, b, c);
}
#pragma endregion Other Operations

#pragma region Individual Coord
mgd::Scalar& Vector3::operator[](int i)
{
	static Scalar dummy;
	switch (i) {
	case 0: return X;
	case 1: return Y;
	case 2: return Z;
	default: assert(0); return dummy;
	}
}

mgd::Scalar Vector3::operator[](int i) const
{
	switch (i) {
	case 0: return X;
	case 1: return Y;
	case 2: return Z;
	default: assert(0); return 0;
	}
}
#pragma endregion Individual Coord

#pragma region Check Conditions
bool Vector3::operator==(const Vector3& b)
{
	return (X == b.X) && (Y == b.Y) && (Z == b.Z);
}
#pragma endregion Check Conditions

#pragma region Versor
const mgd::Vector3 Vector3::right()
{
	return Vector3(1, 0, 0);
}

const mgd::Vector3 Vector3::up()
{
	return Vector3(0, 1, 0);
}

const mgd::Vector3 Vector3::forward()
{
	return Vector3(0, 0, 1);
}

const mgd::Vector3 Vector3::zero()
{
	return Vector3(0, 0, 0);
}
#pragma endregion Versor

mgd::Vector3 Vector3::random(Scalar range)
{
	return Vector3(
		randomBetween(-range, +range),
		randomBetween(-range, +range),
		randomBetween(-range, +range)
	);
}