#include "camera.h"

#pragma region Constructors
mgd::Camera::Camera(Scalar nFocal, int nPixelDimX, int nPixelDimY) :focal(nFocal), pixelDimX(nPixelDimX), pixelDimY(nPixelDimY) {}
#pragma endregion Constructors

mgd::Ray mgd::Camera::primaryRay(int pixelX, int pixelY)
{
	Ray ray;
	ray.point = Point3::zero();
	Scalar clipX = 2.0f * pixelX / pixelDimX - 1.0f;
	Scalar clipY = -2.0f * pixelY / pixelDimY + 1.0f;

	ray.direction = Vector3(clipX, clipY, focal).normalized();
	return ray;
}