#pragma once
#include "vector3.h"

namespace mgd {
	class Camera;
	class GameObject;
	class Quaternion;

	const char* intensityToCstr(Scalar intensity)
	{
		switch (int(round(intensity * 8))) {
		case 0: return "  "; // darkest
		case 1: return " '";
		case 2: return " +";
		case 3: return " *";
		case 4: return " #";
		case 5: return "'#";
		case 6: return "+#";
		case 7: return "*#";
		case 8: return "##"; // lightest
		default:
		case 10: return "##";
		}
	}

	const char* lighting(Versor3 normal)
	{
		Versor3 lightDir = Versor3(1, 2, -2).normalized();

		// lambertian
		Scalar diffuse = Vector3::dot(normal, lightDir);
		if (diffuse < 0) diffuse = 0;

		return intensityToCstr(diffuse);
	}

	void rayCasting(const std::vector<GameObject*>& objectsVector)
	{
		Camera c = Camera(2.0, 64, 64);
		std::string screenBuffer;

		for (int y = 0; y < c.pixelDimY; y++) {
			for (int x = 0; x < c.pixelDimX; x++) {
				Point3 hitPos;
				Point3 hitNorm;
				Scalar distMax = 1000.0;

				for (const GameObject* obj : objectsVector) {
					obj->rayCast(c.primaryRay(x, y), hitPos, hitNorm, distMax);
				}
				screenBuffer += lighting(hitNorm);
			}
			screenBuffer += "\n";
		}

		for (GameObject* obj : objectsVector)	delete obj; 

		std::cout << screenBuffer;
	}

	void render(const Transform& character, const Scene& s, bool isFirstPerson)
	{
		rayCasting(s.toView(character));
	}

	// create interpolation animation for switch first person and third person
	void animation(const Transform& a, const Transform& b, const Scene& s)
	{
		int n = 8;
		Transform transform;
		for (int i = 0; i < n; ++i)
		{
			transform = Transform::lerp(a, b, i * (1.f / n));
			render(transform, s, true);
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
	}

	void handleInput(const Scene& s, Transform** charTransform, Transform& thirdPersonTransform, float movementStep, float turnAngle, bool& firstPerson, 
		const std::vector<GameObject*>& characters)
	{
		Vector3 movement;
		Quaternion turn = Quaternion::identity();

		int input = getchar();

		if (input == ' ')
		{
			static Transform* lastaCharTransform;
			if (firstPerson)
			{
				animation(**charTransform, thirdPersonTransform, s);
				lastaCharTransform = *charTransform;
				*charTransform = &thirdPersonTransform;
			}
			else
			{
				animation(**charTransform, *lastaCharTransform, s);
				*charTransform = lastaCharTransform;
			}
			firstPerson = !firstPerson;
		}

		if (firstPerson && input != '\n')
		{
			if (input >= '0' && input <= '9')
			{
				input -= '0';
				if (input < characters.size())
				{
					if (*charTransform == &characters[input]->transform) return;
					animation(**charTransform, characters[input]->transform, s);
					*charTransform = &characters[input]->transform;
				}
				return;
			}

			switch (input)
			{
				//move input
			case 'w':
				movement = (*charTransform)->forward() * movementStep;
				break;
			case 's':
				movement = -(*charTransform)->forward() * movementStep;
				break;
			case 'a':
				turn = Quaternion::fromAngleAxis(-turnAngle, Vector3::up());
				break;
			case 'd':
				turn = Quaternion::fromAngleAxis(turnAngle, Vector3::up());
				break;
			case 'q':
				movement = (*charTransform)->up() * movementStep;
				break;
			case 'e':
				movement = -(*charTransform)->up() * movementStep;
				break;
			}

			(*charTransform)->move(movement);
			(*charTransform)->turn(turn);
		}
		render(**charTransform, s, firstPerson);
	}
}