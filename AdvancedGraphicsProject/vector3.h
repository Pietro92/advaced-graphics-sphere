#pragma once
#include <assert.h>
#include <cmath>

namespace mgd {
	typedef float Scalar;

	inline Scalar randomBetween(Scalar min, Scalar max) {
		return  min + (std::rand() % 1001) / Scalar(1000) * (max - min);
	}

	inline float lerp(float a, float b, float alpha)
	{
		return a + alpha * (b - a);
	}

	constexpr Scalar TOLERANCE = 1e-5f;

	struct Vector3 {
		Scalar X, Y, Z;

		// constructors
		Vector3();
		Vector3(float _x, float _y, float _z);

		// linear operations
		Vector3 operator*(Scalar k) const;
		void operator*=(Scalar k);
		Vector3 operator/(Scalar k) const;
		void operator/=(Scalar k);
		Vector3 operator+(const Vector3& v) const;
		void operator+=(const Vector3& v);
		Vector3 operator-(const Vector3& v) const;
		void operator-=(const Vector3& v);
		Vector3 operator-() const;
		Vector3 operator+() const;

		/* accesses to individual coordinates as a vector */
		Scalar operator[](int i) const;
		Scalar& operator[] (int i);

		// equals check
		bool operator==(const Vector3& other);

		// norm (aka magnitude, length, intensity, euclidean norm...)
		Scalar squaredNorm() const;
		Scalar norm() const;
		Vector3 normalized() const;
		void normalize();

		static Vector3 random(Scalar range);
		static const Vector3 up();
		static const Vector3 right();
		static const Vector3 forward();
		static const Vector3 zero();

		static Vector3 lerp(const Vector3& v1, const Vector3& v2, float delta);
		static Scalar dot(const Vector3& v1, const Vector3& v2);
		static Vector3 cross(const Vector3& v1, const Vector3& v2);
	};

	inline Vector3 operator*(Scalar k, const Vector3 & a) {
		return a * k;
	}

	inline bool areEqual(Scalar a, Scalar b) {
		return std::abs(a - b) < TOLERANCE;
	}

	inline bool isZero(Scalar a) {
		return std::abs(a) < TOLERANCE;
	}

	inline bool areEqual(const Vector3& v1, const Vector3& v2) {
		return areEqual(v1.X, v2.X) && areEqual(v1.Y, v2.Y) && areEqual(v1.Z, v2.Z);
	}

	inline bool isZero(const Vector3& other) {
		return isZero(other.X) && isZero(other.Y) && isZero(other.Z);
	}

	typedef Vector3 Point3;

	static const Point3 zero()
	{
		return Point3(0, 0, 0);
	}

	typedef Vector3 Versor3;
}