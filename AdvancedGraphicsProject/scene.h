#pragma once
#include <vector>

namespace mgd {
	class GameObject;
	class Transform;

	class Scene {
	public:
		std::vector<GameObject*> objects;

	public:
		Scene() = default;

		void populateWithSpheres(int n);
		void populate();
		std::vector<GameObject*> populateWithCharacters();

		std::vector<GameObject*> toWorld() const;
		std::vector<GameObject*> toView(Transform camera) const;
	};
}