#include <iostream>
#include <chrono>
#include <thread>

#include "vector3.h"
#include "transform.h"
#include "shape3.h"
#include "camera.h"
#include "quaternion.h"
#include "scene.h"
#include "utility_library.h"
#include "unit_test_library.h"

using namespace mgd;

int main()
{
	test::runUnitTests();

	bool isFirstPerson = true;
	const float movementOffset = 1.f;
	const float turnAngle = 25.f;

	Scene s;
	s.populate();
	std::vector<GameObject*> characters = s.populateWithCharacters();

	Transform thirdPerson(Vector3(0, 0, -6));
	Transform* character = &characters[0]->transform;

	while (true)
	{
		handleInput(s, &character, thirdPerson, movementOffset, turnAngle, isFirstPerson, characters);
	}

	return 0;
}