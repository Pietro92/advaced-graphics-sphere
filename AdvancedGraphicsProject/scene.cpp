#include "scene.h"
#include "gameObject.h"
#include "shape3.h"

void mgd::Scene::populateWithSpheres(int n)
{
	for (int i = 0; i < n; i++) {
		Sphere* newSphere = new Sphere();
		newSphere->transform.translate = Vector3::random(14) + Vector3(0, 0, 15);
		newSphere->transform.translate.Y = 0;
		objects.push_back(newSphere);
	}
}

void mgd::Scene::populate()
{
	Sphere* someoneNew = new Sphere(Vector3::zero(), 1, Transform(Vector3(-1, 0, 0)));
	objects.push_back(someoneNew);
	objects.push_back(new Sphere(Vector3::zero(), 1, Transform(Vector3(1, 0, 2))));
	objects.push_back(new Plane(Vector3::zero(), Vector3::up(), Transform(-Vector3::up())));
}

std::vector<mgd::GameObject*> mgd::Scene::populateWithCharacters()
{
	std::vector<GameObject*> characters;

	const int offset = 8;
	const int n = 8;
	const float angle = 360.f / (float)n;
	for (int i = 0; i < n; ++i)
	{
		Quaternion rotation = Quaternion::fromAngleAxis(angle * i, Vector3::up());
		Vector3 position = rotation.apply(Vector3::forward() * offset);

		Transform t(position);

		float fromRotationAngle = fromToRotation(t.forward(), (Vector3::zero() - t.translate).normalized());

		if (Vector3::dot(t.right(), (Vector3::zero() - t.translate).normalized()) < 0)
		{
			fromRotationAngle *= -1;
		}

		t.turn(Quaternion::fromAngleAxis(fromRotationAngle, Vector3::up()));

		Sphere* sphere = new Sphere();
		sphere->transform = t;
		objects.push_back(sphere);
		characters.push_back(sphere);
	}
	return characters;
}

std::vector<mgd::GameObject*> mgd::Scene::toWorld() const
{
	std::vector<GameObject*> res;
	res.clear();

	for (GameObject* obj : objects) {
		res.push_back(obj->applyTransform(obj->transform));
	}
	return res;
}

std::vector<mgd::GameObject*> mgd::Scene::toView(Transform camera) const
{
	std::vector<GameObject*> res;
	res.clear();

	for (GameObject* obh : objects)
	{
		GameObject* current = obh->applyTransform(obh->transform);
		res.push_back(current->applyTransform(camera.inverse()));
		delete current;
	}
	return res;
}
