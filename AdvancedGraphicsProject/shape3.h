#pragma once
#include "gameobject.h"
#include "vector3.h"

namespace mgd {
	class Transform;

	struct Ray {
		Point3 point;
		Versor3 direction;

		Ray() : point(Point3()), direction(Vector3::zero()) {}
		Ray(Point3 nPoint, Vector3 nDirection) : point(nPoint), direction(nDirection.normalized()) {}
	};

	class Sphere : public GameObject {
	public:
		Point3 center;
		Scalar radius;

	public:
		Sphere(Point3 nCenter = Vector3::zero(), Scalar nRadius = 1, Transform nTransform = Transform());
		
		virtual void rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const override;
		virtual GameObject* applyTransform(const Transform& nTransform) const override;
	};

	class Plane : public GameObject {
	public:
		Point3 point;
		Versor3 normal;

	public:
		Plane(Point3 nPoint, Versor3 nNormal, Transform nTransform = Transform());
		
		virtual void rayCast(const Ray& ray, Point3& hitPos, Versor3& hitNorm, float& distMax) const override;
		virtual GameObject* applyTransform(const Transform& nTransform) const override;
	};

	inline bool rayCast(const Ray& ray, const Sphere& sphere, Point3& hitPos, Versor3& hitNorm, float& distMax)
	{
		Scalar a = 1;
		Scalar b = 2 * Vector3::dot(ray.direction, ray.point - sphere.center);
		Scalar c = (ray.point - sphere.center).squaredNorm() - pow(sphere.radius, 2);

		Scalar delta = b * b - 4 * a * c;

		if (delta < 0) return false; // ray misses the sphere!

		Scalar k = (-b - sqrt(delta)) / (2 * a);
		if (k < 0) return false;
		if (k > distMax) return false;
		distMax = k;

		hitPos = ray.point + k * ray.direction;
		hitNorm = (hitPos - sphere.center).normalized();
		return true;
	}

	inline bool rayCast(const Ray& ray, const Plane& plane, Point3& hitPos, Versor3& hitNorm, float& distMax)
	{
		Scalar dn = Vector3::dot(ray.direction, plane.normal);
		if (dn == 0) return false;

		Scalar k = Vector3::dot(plane.point - ray.point, plane.normal) / dn;

		if (k < 0) return false;
		if (k > distMax) return false;
		distMax = k;
		hitPos = ray.point + k * ray.direction;
		hitNorm = plane.normal;
		return true;
	}
}
