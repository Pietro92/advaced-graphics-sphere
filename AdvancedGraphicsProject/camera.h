#pragma once
#include "vector3.h"
#include "shape3.h"

namespace mgd {
	struct Ray;

	class Camera {
	public:
		Scalar focal;
		int pixelDimX, pixelDimY;

	public:
		Camera(Scalar nFocal, int nPixelDimX, int nPixelDimY);

		Ray primaryRay(int pixelX, int pixelY);
	};
}
